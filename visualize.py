#!/usr/bin/env python3
# université pierre et marie curie - c. dürr - 2016

"""
Usage:
    ./visualize.py < inputfile  > visualize.js

Lit un texte avec des données géographiques et produit du code javascript pour
la visualisation. L'entrée contient trois types de lignes.  Une ligne de la
forme

    v identifiant longitude latitude

décrit un sommet, où *identifiant* est un identifiant unique du sommet et
*longitude, latitude* sont des nombres flottants qui représentent les degrés
des coordonnées GPS. Une ligne de la forme

    s longitude latitude

représente un sommet sélectionné. Alors qu'une ligne

    0 valeur_objectif

décrit la valeur objectif de la solution.
"""

from math import sin, cos, sqrt, radians, asin
import sys


# --- from tryalgo.convex_hull import andrew

def left_turn(a, b, c):
    return (a[0] - c[0]) * (b[1] - c[1]) - (a[1] - c[1]) * (b[0] - c[0]) > 0


def andrew(S):
    """Convex hull by Andrew

    :param S: list of points as coordinate pairs
    :requires: S has at least 2 points
    :returns: list of points of the convex hull
    :complexity: `O(n log n)`
    """
    S.sort()
    top = []
    bot = []
    for p in S:
        while len(top) >= 2 and not left_turn(p, top[-1], top[-2]):
            top.pop()
        top.append(p)
        while len(bot) >= 2 and not left_turn(bot[-2], bot[-1], p):
            bot.pop()
        bot.append(p)
    return bot[:-1] + top[:0:-1]

# --- données globales

GPS = {}           # associe identifiant à un point (latitude, longitude)
selected = []      # liste de centres
objvalue = 0.0     # valeur objectif obtenue par la liste des centres


def dist(p, q):
    """distance à vol d'oiseau entre deux sommets
    """
    dlon = p[1] - q[1]
    dlat = p[0] - q[0]
    a = sin(radians(dlat / 2))**2 + cos(radians(p[0])) * cos(radians(q[0])) * sin(radians(dlon / 2))**2
    return asin( sqrt(a) )


def distList(a, L):
    '''distance entre un point a et une liste de points L
    retourne le couple (mindist, argmin)
    '''
    assert L != []
    return min((dist(a, L[i]), i) for i in range(len(L)))


"""lit de l'entrée standard une solution dont les lignes sont dans un des formats
v [identificateur] [latitude] [longitude]
s [latitude] [longitude]
o [valeur objectif]
"""
for line in sys.stdin:
    if line[0] == "v":
        tab = line.split()
        identficateur = int(tab[1])
        latitude  = float(tab[2])
        longitude = float(tab[3])
        GPS[identficateur] = (latitude, longitude)
    if line[0] == 's':
        tab = line.split()
        latitude  = float(tab[1])
        longitude = float(tab[2])
        selected.append((latitude, longitude))
    if line[0] == "o":
        tab = line.split()
        objvalue = float(tab[1])

S = range(len(selected))       # indices des centres
cell = [[] for s in S]         # associe à un indice de centre la liste des points qui lui sont associés

# assign points to closest selected point
for i in GPS:
    _, a = distList(GPS[i], selected)
    cell[a].append(GPS[i])

# restrict to convex hull
for j in S:
    cell[j] = andrew(cell[j])

# --- print javascript for visualization

print ("var markers = [")
for p in selected:
    print(" [%.6f, %.6f]," % p)
print("];")


print("var polygons = [")
for j in S:
    print("[")
    for point in cell[j]:
        print("[%.6f, %.6f], " % point )
    print("[%.6f, %.6f] " % cell[j][0] )   # to close the polygon
    print("],")
print("];")

print("var objvalue = %.10f;" % objvalue)
