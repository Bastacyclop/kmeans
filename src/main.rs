extern crate rand;

use std::{io, str, fs, env};
use rand::Rng;

#[derive(Debug, Clone)]
struct Vertex {
    latitude: f32,
    longitude: f32,
}

impl Vertex {
    fn zero() -> Self {
        Vertex {
            latitude: 0.,
            longitude: 0.,
        }
    }

    fn from_input(s: &str) -> Self {
        let mut words = s.split_whitespace().skip(2);
        Vertex {
            latitude: words.next().expect("expected latitude")
                .parse().expect("could not parse latitude"),
            longitude: words.next().expect("expected longitude")
                .parse().expect("could not parse longitude"),
        }
    }

    fn output<W: io::Write>(&self, w: &mut W) {
        writeln!(w, "s {} {}", self.latitude, self.longitude)
            .expect("could not output vertex");
    }
}

fn haversine_distance(v1: &Vertex, v2: &Vertex) -> f32{
    const SPHERE_RADIUS: f32 = 1.0;
    let lat1 = v1.latitude.to_radians();
    let lon1 = v1.longitude.to_radians();
    let lat2 = v2.latitude.to_radians();
    let lon2 = v2.longitude.to_radians();

    let dlat = lat2 - lat1;
    let dlon = lon2 - lon1;
    let a = (dlat / 2.0).sin().powi(2)
          + lat1.cos() * lat2.cos() * (dlon / 2.0).sin().powi(2);
    let c = 2.0 * a.sqrt().atan2((1.0 - a).sqrt());
    SPHERE_RADIUS * c
}

struct Cluster {
    center: Vertex,
    accumulator: Vertex,
    accumulated: usize,
    centroid: Vertex,
}

impl Cluster {
    fn with_center(c: &Vertex) -> Self {
        Cluster {
            center: c.clone(),
            accumulator: Vertex::zero(),
            accumulated: 0,
            centroid: Vertex::zero(),
        }
    }

    fn accumulate(&mut self, v: &Vertex) {
        self.accumulator.latitude += v.latitude;
        self.accumulator.longitude += v.longitude;
        self.accumulated += 1;
    }

    fn compute_centroid(&mut self) {
        self.centroid.latitude = self.accumulator.latitude
            / self.accumulated as f32;
        self.centroid.longitude = self.accumulator.longitude
            / self.accumulated as f32;
        self.accumulator = Vertex::zero();
        self.accumulated = 0;
    }
}

fn initialize_clusters(vertices: &Vec<Vertex>, k: usize) -> Vec<Cluster> {
    let mut rng = rand::thread_rng();
    let mut clusters = Vec::with_capacity(k);
    clusters.push(Cluster::with_center(rng.choose(vertices).unwrap()));
    let mut weights = Vec::with_capacity(vertices.len());

    for _ in 1..k {
        for v in vertices {
            weights.push(clusters.iter().fold(1f32/0., |m, c| {
                m.min(haversine_distance(&c.center, v).powi(2))
            }));
        }

        let mut total_weight = 0.;
        for w in &mut weights {
            total_weight += *w;
            *w = total_weight;
        }

        let sample_weight = rng.gen_range(0., total_weight);
        let mut i = 0;
        // TODO: dichotomy would be better
        while weights[i] <= sample_weight { i += 1; }

        clusters.push(Cluster::with_center(&vertices[i]));
        weights.clear();
    }

    clusters
}

fn main() {
    use std::io::BufRead;

    let mut args = env::args().skip(1);
    let k = args.next().expect("expected k paramater")
        .parse().expect("could not parse k paramater");
    let path = args.next().expect("expected input file");
    let file = fs::File::open(path).expect("could not open input file");
    let mut line = String::new();
    let mut reader = io::BufReader::new(file);

    let mut vertices = Vec::new();
    while reader.read_line(&mut line).unwrap() > 0 {
        print!("{}", line);
        vertices.push(Vertex::from_input(&line));
        line.clear();
    }

    let mut clusters = initialize_clusters(&vertices, k);

    let mut vertex_clusters = Vec::with_capacity(vertices.len());
    loop {
        // assign vertices to clusters
        for v in &vertices {
            let (index, _) = clusters.iter().enumerate()
                .fold((0, 1f32/0.), |(mi, md), (i, c)| {

                let d = haversine_distance(&c.center, v).powi(2);
                if d < md { (i, d) } else { (mi, md) }
            });
            vertex_clusters.push(index);
            clusters[index].accumulate(v);
        }

        for cluster in &mut clusters {
            cluster.compute_centroid();
        }

        let mut changed = false;
        for (v, &ci) in vertices.iter().zip(vertex_clusters.iter()) {
            let cluster = &mut clusters[ci];
            // TODO: precompute, use square ?
            let cd = haversine_distance(&cluster.centroid, &cluster.center);
            let vd = haversine_distance(&cluster.centroid, v);
            if vd < cd {
                cluster.center = v.clone();
                changed = true;
            }
        }

        if !changed { break; }
        vertex_clusters.clear();
    }

    let mut sum = 0.;
    for cluster in &clusters {
        cluster.center.output(&mut io::stdout());
        for v in &vertices {
            sum += haversine_distance(&cluster.center, v).powi(2)
        }
    }

    println!("o {}", sum);
}
